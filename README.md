# Tormenta20

Tormenta20 is a brazilian system owned by Jambo Editora.

## Description

Build campaigns in the Tormenta20 using Foundry VTT!

## TODOS

* Sheet Layout
* Spell memorized check
* Add Oficios variations
* Add tab for bio and picture

## Special Thanks

Special Thanks to Asacolips for his System Development Tutorial and DungeonWorld system which I used as reference during the development of this system.